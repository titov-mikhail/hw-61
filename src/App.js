import './App.css';
import SelectCountry from "./components/Containers/SelectCountry/SelectCountry";
import ShowCountryInfo from "./components/ShowCountryInfo/ShowCountryInfo";
import {useEffect, useState} from "react";
import axios from "axios";
const url = 'https://restcountries.com/v2/';

function App() {
  const [country, setCountry] = useState(null);
  const [allCountries, setAllCountries] = useState([]);


  const getRequest = async (url) => {
      return await axios.get(url);
  }
  const getCountries = async () => {
     const getCountriesUrl = url + 'all?fields=alpha3Code&fields=name';
     const resp = await getRequest(getCountriesUrl);
     setAllCountries(resp.data);
  }

  const getCountryInfo = async (countryCode) => {
      const getUrl = url + `alpha/${countryCode}`;
      const selectedCountry  = await getRequest(getUrl);
      Promise
         .resolve(selectedCountry.data)
         .then(resp =>
         {
             setCountry(resp);
         });
  }

  useEffect(()=>{
      async function fetchData () {
          await getCountries();
      }
      fetchData();
  }, []);

  return (
    <div className="App">
        <SelectCountry
            countries = {allCountries}
            onCountrySelected = {async (code)=> await getCountryInfo(code)}
        />

        <ShowCountryInfo
            key = {country ? country.alpha3Code : null}
            country ={country}
            baseUrl = {url}
            getRequest = {(urlReq) => getRequest(urlReq)}
        />
    </div>
  );
}

export default App;
