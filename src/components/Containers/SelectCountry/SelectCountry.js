import React from 'react';
import './SelectCountry.css';
import {Button} from "@mui/material";

function SelectCountry(props) {
    return (
        <div className='SelectCountry'>
            {props.countries.map(c => {
            return <Button
                key={c.alpha3Code}
                variant="text"
                onClick={async ()=>await props.onCountrySelected(c.alpha3Code)}
            >
                {c.name}
            </Button>
        })}
        </div>
    );
}

export default SelectCountry;