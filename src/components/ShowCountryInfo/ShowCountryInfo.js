import React, {useEffect, useState} from 'react';
import './ShowCountryInfo.css';

function ShowCountryInfo (props) {
    const [borderCountries, setBorderCountries] = useState([]);

    const getBorderCountries = async (borders) => {
        const promises = borders.map(code=>getCountryName(code));
        let countries = [];
          await Promise
            .all(promises)
            .then(resp=> countries = resp);

        return  countries.map(c => c.data.name );
    }

    const getCountryName = async (countryCode) => {
        const borderCountryUrl =  `${props.baseUrl}alpha/${countryCode}?fields=name`
        return props.getRequest(borderCountryUrl);
    }

    useEffect(async ()=>{
        if(props.country && props.country.borders){
            const bc = await getBorderCountries(props.country.borders);
            setBorderCountries(bc);
        } else {
            setBorderCountries(['No border countries']);
        }
    }, [])

    return (
            <>
                {!props.country
                    ? <h1>Выберите страну</h1>
                    :  <div className='ShowCountryInfo'>
                        <div>
                            <h1>{props.country.name}</h1>
                            <h2>Capital: {props.country.capital}</h2>
                            <h3>Population: {props.country.population}</h3>
                            <h4>Borders with</h4>
                            <ol>
                                {borderCountries.map(bc=>{
                                   return <li key={bc}>{bc}</li>
                                })}
                            </ol>
                        </div>
                        <div className='Flag'>
                               <img src={props.country.flags.png} alt='Flag'/>
                        </div>
                    </div>
                }
            </>
    );
}

export default ShowCountryInfo;